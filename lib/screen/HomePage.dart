import 'package:clockin_id/provider/TextShow.dart';
import 'package:clockin_id/theme.dart';
import 'package:clockin_id/widget/Dashboard.dart';
import 'package:clockin_id/widget/Settings.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentab = 0;
  final List<Widget> screens = [
    Dashboard(),
    Setting(),
  ];

  Widget currentScreen = Dashboard();

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6EFEF),
      // body: Dashboard(),
      body: ChangeNotifierProvider<TextShow>(
        create: (context) => TextShow(),
        child: PageStorage(
          bucket: bucket,
          child: currentScreen,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Absensi',
        onPressed: () {},
        child: Icon(Icons.phone_android),
        backgroundColor: orangeColor,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: primaryColor,
        notchMargin: 10,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    currentScreen = screens[0];
                    currentab = 0;
                  });
                },
                child: Column(
                  children: [
                    Icon(
                      Icons.dashboard,
                      size: 40,
                      color: currentab == 0 ? Colors.white : Color(0xFFA9D2DC),
                    ),
                    Text(
                      'Dashboard',
                      style: TextStyle(
                        fontSize: 14,
                        color:
                            currentab == 0 ? Colors.white : Color(0xFFA9D2DC),
                      ),
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    currentScreen = screens[1];
                    currentab = 1;
                  });
                },
                child: Column(
                  children: [
                    Icon(
                      Icons.settings,
                      size: 40,
                      color: currentab == 1 ? Colors.white : Color(0xFFA9D2DC),
                    ),
                    Text(
                      'Settings',
                      style: TextStyle(
                        fontSize: 14,
                        color:
                            currentab == 1 ? Colors.white : Color(0xFFA9D2DC),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
