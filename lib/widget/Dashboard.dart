import 'package:clockin_id/theme.dart';
import 'package:clockin_id/widget/BoxInfo.dart';
import 'package:flutter/material.dart';

import 'Header.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({
    Key key,
  }) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Header(),
        SizedBox(
          height: 35,
        ),
        BoxInfo(),
        SizedBox(
          height: 10,
        ),
        Expanded(
          child: GridView.count(
            padding: EdgeInsets.all(20),
            crossAxisSpacing: 15.0,
            mainAxisSpacing: 15.0,
            crossAxisCount: 2,
            children: [
              GridItem(
                iconData: Icons.calendar_today,
                menu: 'Attendace',
              ),
              GridItem(
                iconData: Icons.directions_run,
                menu: 'On Leave',
              ),
              GridItem(
                iconData: Icons.alarm,
                menu: 'Overtime',
              ),
              GridItem(
                iconData: Icons.card_travel,
                menu: 'On Trip',
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class GridItem extends StatelessWidget {
  final IconData iconData;
  final String menu;

  GridItem({this.iconData, this.menu});

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Stack(
        children: [
          Positioned(
            right: -40,
            bottom: -40,
            child: Container(
              width: 110,
              height: 110,
              decoration: BoxDecoration(
                color: primaryColor.withOpacity(0.5),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  iconData,
                  size: 80,
                ),
                SizedBox(height: 10),
                Text(
                  menu,
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
          )
        ],
      ),
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }
}
