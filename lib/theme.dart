import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color primaryColor = Color(0xFF64B6CB);
Color secondaryColor = Color(0xFF2A7489);
Color orangeColor = Color(0xFFFF6633);

TextStyle nameProfile = GoogleFonts.poppins(
  color: Colors.white,
  fontWeight: FontWeight.w500,
  fontSize: 12,
);

TextStyle subNameProfile = GoogleFonts.poppins(
  color: Colors.white,
  fontWeight: FontWeight.w500,
  fontSize: 10,
);

TextStyle namaPerusahaan = GoogleFonts.poppins(
  color: Colors.white.withOpacity(0.72),
  fontWeight: FontWeight.w600,
  fontSize: 10,
);

TextStyle textPrimary = GoogleFonts.poppins(
  color: primaryColor,
  fontSize: 12,
);

TextStyle textItem = GoogleFonts.poppins(
  color: Colors.black,
  fontSize: 12,
);

TextStyle textMenu = GoogleFonts.poppins(
  color: Colors.white,
  fontSize: 14,
);
