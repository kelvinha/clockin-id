import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class TextShow with ChangeNotifier {
  bool _showText = true;

  bool get showText => _showText;
  set showText(bool value) {
    _showText = value;
    notifyListeners();
  }
}
